// Library Imports
import {Fragment} from 'react';
import {Container, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';


// Components Imports
import ShowAllProducts from './ShowAllProducts.js';

export default function AdminDashboard(){


	return(
		<Fragment>
			<Container className = "mt-5 text-center">
				<h1>Admin Dashboard</h1>

				<div className = "mt-3 d-flex justify-content-center">
					<Button as = {Link} to = {`/products/addProduct`} className = "mx-2">Add New Product</Button>
					{/*<Button as = {Link} to = {`/order/viewAllOrders`} className = "mx-2 border-success" variant = "success">Show User Orders</Button>*/}
				</div>

				<ShowAllProducts/>
			</Container>
		</Fragment>
		);
}