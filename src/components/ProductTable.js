// Library Imports
import {Fragment, useState} from 'react';
import {Button, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {useNavigate} from 'react-router-dom';


// Components Imports
// import UserContext from '../UserContext.js';
// import UpdateProductModal from './UpdateProductModal';


export default function ProductTable({productProp}){

	const {_id, name, description, price, isActive} = productProp;

	const [productName, setProductName] = useState("");
	const [productDescription, setProductDescription] = useState("");
	const [productPrice, setProductPrice] = useState("");
	const [productImage, setProductImage] = useState("");

	// const {user} = useContext(UserContext);
	const navigate = useNavigate();

	// const [isAvailable, setIsAvailable] = useState("");

	// For Modal
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);



	// Update Product Function
	function updateProduct(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_SOOBOONIT}/products/updateProduct/${_id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: productName,
				description: productDescription,
				price: productPrice,
				image: productImage
			})
		})
		.then(result => result.json())
		.then(data => {
			// console.log(data);

			if(data){
				Swal.fire({
					title: "Product Updated!",
					icon: "success",
					text: "You can now view the updated product in the products list."
				})

				navigate('/admin');
			}
			else{
				Swal.fire({
					title: "Product Update Failed",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}



	// Disable Product Function
	function disableProduct(event){
		fetch(`${process.env.REACT_APP_SOOBOONIT}/products/archiveProduct/${_id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(result => result.json())
		.then(data => {
			// console.log(data);

			if(data){
				Swal.fire({
					title: "Product Disabled Successfully",
					icon: 'success',
					text: "The product has been disabled successfully"
				})

				navigate('/admin');
			}
			else{
				Swal.fire({
					title: "Product Disable Failed",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}


	// /Enable/Reactivate Product Function
	function enableProduct(event){
		fetch(`${process.env.REACT_APP_SOOBOONIT}/products/reactivateProduct/${_id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(result => result.json())
		.then(data => {
			// console.log(data);

			if(data){
				Swal.fire({
					title: "Product Reactivated Successfully",
					icon: 'success',
					text: "The product has been reactivated successfully"
				})

				navigate('/admin');
			}
			else{
				Swal.fire({
					title: "Product Reactivate Failed",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}



	return(
		<Fragment>
			<tr>
			  <td>{name}</td>
			  <td>{description}</td>
			  <td>{price}</td>

			  {/*To Check is the Product is "Available" or "Not Available"*/}
			  {
			  	isActive?
			  	<td>Available</td>
			  	:
			  	<td>Not Available</td>
			  }

			  <td>
				{/*<UpdateProductModal />		  	Code for Modal*/}

			  	<Fragment>
				  	<Button
				  		variant="primary"
				  		className = "mx-2"
				  		onClick={handleShow}>
				  	        Update
				  	</Button>

				  	<Modal
				  	   show={show}
				  	   onHide={handleClose}
				  	   backdrop="static"
				  	   keyboard={false}
				  	 >
				  	   <Modal.Header closeButton>
				  	     <Modal.Title>Update Product Form</Modal.Title>
				  	   </Modal.Header>
				  	   <Modal.Body>
				  	   		<Form>
				  	   		    <Form.Group className="mb-3" controlId="formBasicEmail">
				  	   		        <Form.Label>Product Name</Form.Label>
				  	   		        <Form.Control
				  	   		        type="text"
				  	   		        placeholder = "Enter Updated Name"
				  	   		        value = {productName}
				  	   		        onChange = {event => setProductName(event.target.value)}
				  	   		        required/>
				  	   		    </Form.Group>

				  	   		    <Form.Group className="mb-3" controlId="formBasicPassword">
				  	   		        <Form.Label>Product Description</Form.Label>
				  	   		        <Form.Control
				  	   		        type="text"
				  	   		        placeholder = "Enter Updated Description"
				  	   		        value = {productDescription}
				  	   		        onChange = {event => setProductDescription(event.target.value)}
				  	   		        required/>
				  	   		    </Form.Group>

				  	   		    <Form.Group className="mb-3" controlId="formBasicPassword">
				  	   		        <Form.Label>Price</Form.Label>
				  	   		        <Form.Control
				  	   		        type="number"
				  	   		        placeholder = "Enter Updated Price"
				  	   		        value = {productPrice}
				  	   		        onChange = {event => setProductPrice(event.target.value)}
				  	   		        required/>
				  	   		    </Form.Group>

				  	   		    <Form.Group className="mb-3" controlId="formBasicPassword">
				  	   		        <Form.Label>Product Image</Form.Label>
				  	   		        <Form.Control
				  	   		        type="text"
				  	   		        placeholder = "Enter Updated Image Link"
				  	   		        value = {productImage}
				  	   		        onChange = {event => setProductImage(event.target.value)}
				  	   		        required/>
				  	   		    </Form.Group>

				  	   		</Form>
				  	   </Modal.Body>
				  	   <Modal.Footer>
				  	     <Button variant="secondary" onClick={handleClose}>
				  	       Close
				  	     </Button>
				  	     <Button variant="primary" onClick = {updateProduct}>Update</Button>
				  	   </Modal.Footer>
				  	</Modal>
			  	</Fragment>


			  	{/*For Disable or Enable Buttons*/}
			  	{
			  		isActive?
			  		<Button variant = "warning" className = "mx-2" onClick = {disableProduct}>Disable</Button>
			  		:
			  		<Button variant = "success" className = "mx-2" onClick = {enableProduct}>Enable</Button>
			  	}
			  </td>

			  {/*<td key = {1} variant = "primary">Update&nbsp;</td>
			  <td key = {2} variant = "success">Disable&nbsp;</td>*/}
			</tr>


			
		</Fragment>
		);
}