// Library Imports
// import {Fragment} from 'react';
// import {Table} from 'react-bootstrap';

// Components Imports


export default function OrderTable(orderProp){

	const {_id, userId, productId, quantity, totalAmount, purchasedOn} = orderProp;
	// console.log(orderProp);

	return(
			<tr>
				<td>{_id}</td>
				<td>{userId}</td>
				<td>{productId}</td>
				<td>{quantity}</td>
				<td>{totalAmount}</td>
				<td>{purchasedOn}</td>
			</tr>
		);
}